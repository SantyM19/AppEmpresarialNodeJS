import {
  addComment,
  editComment,
  listComments,
  removeComment,
  listAll
} from '../use-cases'
import makeDeleteComment from './delete-comment'
import makeGetComments from './get-comments'
import makePostComment from './post-comment'
import makePatchComment from './patch-comment'
import notFound from './not-found'
import makeGetAllComments from './get-all'

const deleteComment = makeDeleteComment({ removeComment })
const getComments = makeGetComments({
  listComments
})

const getAll = makeGetAllComments({
  listAll
})

const postComment = makePostComment({ addComment })
const patchComment = makePatchComment({ editComment })

const commentController = Object.freeze({
  deleteComment,
  getComments,
  notFound,
  getAll,
  postComment,
  patchComment
})

export default commentController
export { deleteComment, getComments, notFound, postComment, patchComment, getAll }
