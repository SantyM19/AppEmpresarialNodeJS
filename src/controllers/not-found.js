export default async function notFound () {
  return {
    headers: {
      'Content-Type': 'application/json'
    },
    body: { error: 'No encontrado.' },
    statusCode: 404
  }
}
